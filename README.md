## GitLab CI/CD pipeline

## Description
CI/CD pipeline configuration files.

## Usage
These configuration files are for building and running a GitLab CI/CD pipeline.